# DOTFILES FOR XMONAD RICE

#### DOTFILES ALSO EXIST FOR ALACRITTY, VIM, BASH

This directory contains the personal set dotfiles that I create for my manjaro xmonad configuration.

The software to install during arch install are:
1. pacstrap - base, linux, linux-firmware, gvim, git
1. pacman -[after chroot in the system] grub, xterm, alacritty, man-db, man-pages, texinfo, cryptsetup, device-mapper, dhcpd, diffutils, e2fsprogs, inetutils, jfsutils, less, logrotate, lvm2, mdadm, networkmanager, perl, reiserfsprogs, s-nail, sysfsutils, usbutils, which, mlocate, xfsprogs, sudo.
1. xorg, pulseaudio, pulseaudio-alsa, xorg-xinit, xorg-server,
1. xmonad, xmonad-contrib, cabal, lightdm, lightdm-gtk-greeter, gufw

* put the alacritty folder in the ~/.config/ directory as is
* put the .vimrc in the home folder and install plug and also create the .vim directory
* put the bash files in the home folder

check out this video after 7 minute mark for a list of must have softwares after fresh arch install
https://www.youtube.com/watch?v=tMd9-71zAio&ab_channel=DistroTube


* Themes 
  
  - gtk theme:
  - icon themes:
	  * Flattery teal dark
	  * Vimix doder
	  * Vimix ruby
	  * Vimix beryl
	  * Tela icon themes
  - cursor themes:
	  * Vimix cursor
	


